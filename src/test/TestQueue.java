package test;

import model.data_structures.Queue;
import junit.framework.TestCase;




public class TestQueue extends TestCase {

	private Queue<String> lista;
	private void setupEscenario1(){
		lista=new Queue<String>();
		lista.enqueue("Falcao");
		lista.enqueue("James");
		lista.enqueue("Mi�ia");
		lista.enqueue("Cuadrado");
	}
	private void setupEscenario2(){
		lista=new Queue<String>();
	}
	private void setupEscenario3(){
		lista=new Queue<String>();
		lista.enqueue("Dybala");
	}

	public void testEnqueue(){
		setupEscenario1();

		int numero=lista.size();
		lista.enqueue("Ospina");
		assertEquals( lista.size(),numero+1);
	

		lista.enqueue("Ronaldo");
		assertEquals( lista.size(),numero+2);

		setupEscenario2();
		lista.enqueue("Insigne");
		assertEquals(lista.size(),1);

		setupEscenario3();
		lista.enqueue("Griezmann");
		assertEquals(lista.size(),2);
	}

	public void testDequeue(){
		setupEscenario1();

		int numero=lista.size();
		lista.dequeue();
		assertEquals( lista.size(),numero-1);
		System.out.println(numero-1);
		System.out.println(lista.getFirst().getData());




	}

}
