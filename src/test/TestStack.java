package test;


import model.data_structures.Stack;
import junit.framework.TestCase;

public class TestStack extends TestCase{
	

	private Stack<String> lista;
	private void setupEscenario1(){
		lista=new Stack<String>();
		lista.push("Falcao");
		lista.push("James");
		lista.push("Cuadrado");
	}
	private void setupEscenario2(){
		lista=new Stack<String>();
	}
	private void setupEscenario3(){
		lista=new Stack<String>();
		lista.push("Dybala");
	}

	public void testPush(){
		setupEscenario1();

		int numero=lista.size();
		lista.push("Ospina");
		assertEquals( lista.size(),numero+1);
	

		lista.push("Ronaldo");
		assertEquals( lista.size(),numero+2);

		setupEscenario2();
		lista.push("Insigne");
		assertEquals(lista.size(),1);

		setupEscenario3();
		lista.push("Griezmann");
		assertEquals(lista.size(),2);
	}

	public void testPop(){
		setupEscenario1();

		int numero=lista.size();
		lista.pop();
		assertEquals( lista.size(),2);
		System.out.println(numero-1);
		System.out.println(lista.getFirst().getItem());




	}


}
