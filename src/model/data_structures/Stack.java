package model.data_structures;

import java.util.Iterator;

public class Stack<T>implements ILista<T> {

	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private StackNodo<T> first;
	
	private int size;
	
	public Stack(){
		
		size=0;
		
		
	}
	
	public StackNodo<T> getFirst() {
		return first;
	}
	
	public void push(T item){
		
		StackNodo<T> nodo = new StackNodo<T>();
		nodo.setItem(item);
		if(first == null){
			first = nodo;
			size++;
			return;
		}else{
			nodo.setNext(first);
			first = nodo;
			size++;
		}
	}
	
	public T pop(){
		StackNodo<T> temp = first;
		if(first.getNext() == null){
			first = null;
			size--;
			
		}else{
			first = first.getNext();
			size--;
		}
		return temp.getItem();
	}
	
	public boolean isEmpty(){
		return(first == null);
	}
	public int size(){
		return size;
	}
	

}
