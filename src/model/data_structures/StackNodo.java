package model.data_structures;

public class StackNodo<T> extends Stack<T> {

	private T item;
	
	private StackNodo<T> next;

	public T getItem() {
		return item;
	}

	public void setItem(T item) {
		this.item = item;
	}

	public StackNodo<T> getNext() {
		return next;
	}

	public void setNext(StackNodo<T> next) {
		this.next = next;
	};
}
