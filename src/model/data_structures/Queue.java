package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements ILista<T>  {

	private QueueNodo<T> first ;

	private QueueNodo<T> last ;
	
	private int size;

	public QueueNodo<T> getFirst() {
		return first;
	}

	public QueueNodo<T> getLast() {
		return last;
	}


	
	// George deber�a implementar el mismo iterador del taller 2?
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	public Queue(){

		first = null;

		last = null;
		
		size = 0;

	}

	public void enqueue(T item){

		QueueNodo<T> nodo = new QueueNodo<T>();
		nodo.setData(item);
		if(isEmpty()){
			
			// verificar si �ste nodo.setNext es correcto
			first = nodo;
			last = nodo;
			size++;
			return;
		}else{
			last.setNext(nodo);
			last = nodo;
			last.setNext(null);
			size++;
		}
	}

	// revisar
	public T dequeue(){

		QueueNodo<T> temp = first;
		if(first.getNext() != null){
			first = first.getNext();
			size--;
		}else{
			first = null;
		}
		return temp.getData();

	}

	public boolean isEmpty(){

		return (first == null);
	}

	public int size(){

		return size;
	}

}
