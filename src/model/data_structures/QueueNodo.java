package model.data_structures;

public class QueueNodo <T> extends Queue<T> {
	
	private  T data;
	
	private QueueNodo<T> next;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public QueueNodo<T> getNext() {
		return next;
	}

	public void setNext(QueueNodo<T> next) {
		this.next = next;
	}
	
	
	
	

}
