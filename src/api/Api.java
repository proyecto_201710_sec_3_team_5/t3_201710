package api;

import model.data_structures.Stack;



	public interface Api
	{
		
		public boolean expresionBienFormada (String expresion);
		
		public Stack<Character> ordenPila();
		
		public Stack<Character> getStack();
	}

